﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public float turnMovement = 0;
    public float throttleMovement = 0;

    public bool isPaused = false;
	

	void Update () {

        turnMovement = Input.GetAxisRaw("Horizontal");
        throttleMovement = Input.GetAxisRaw("Vertical");

    }
}
