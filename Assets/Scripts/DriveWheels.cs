﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriveWheels : MonoBehaviour {

    public float maxTurn = 60;
    public float minTurn;
    public float turnSpeed; //Degrees per second
    public float rotation;
    // Use this for initialization
    void Start () {
        minTurn = -maxTurn;
        rotation = 0;
        turnSpeed = 120;
	}
	
	// Update is called once per frame
	void Update () {
        RotateWheel();
	}

    public void RotateWheel()
    {
        rotation += StaticGM.input.turnMovement * turnSpeed * Time.deltaTime;
        rotation = Mathf.Clamp(rotation, minTurn, maxTurn);
        transform.localEulerAngles = new Vector3(0, rotation, 0);
    }
}
