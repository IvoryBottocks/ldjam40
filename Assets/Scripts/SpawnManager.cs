﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public GameObject[] spawnPoints;

    public GameObject objToSpawn;

	void Start () {
		
	}

    public void SpawnTrailer()
    {
        Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform;

        for (int i = 0; i < spawnPoints.Length; i++)
        {
            if (!spawnPoints[i].gameObject.GetComponent<SpawnPoint>().isOcupied)
            {
                Instantiate(objToSpawn, spawnPoint);
                break;
            }
            else
                Debug.Log("No place to put trailer!");
        }
    }


}
