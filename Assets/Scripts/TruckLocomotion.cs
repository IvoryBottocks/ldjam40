﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckLocomotion : MonoBehaviour {

    [SerializeField]
    private DriveWheels frontRight, frontLeft;

    private Rigidbody truckRB;
    public float wheelAngle;
    public float truckRotationSpeed;
	public bool isReversing;

	// Use this for initialization
	void Start () {
        truckRotationSpeed = 0.5f;
        truckRB = GetComponent<Rigidbody>();
		isReversing = false;
	}
	
	void FixedUpdate () {
		wheelAngle = (frontRight.rotation - (transform.rotation.y));
		isReversing = Vector3.Dot (transform.forward, truckRB.velocity) < 0;
		int revFactor = isReversing ? -1 : 1;

		// Rotate very slowly when almost stationary...
		float rotateMagnitude = Mathf.Min(truckRB.velocity.magnitude, 1.0f);
		transform.Rotate(0, revFactor * rotateMagnitude * wheelAngle * truckRotationSpeed * Time.deltaTime, 0);

		Vector3 forceDirection = frontRight.transform.forward;
		print ("ForceDirection: " + forceDirection);
		truckRB.AddForce(forceDirection * StaticGM.input.throttleMovement, ForceMode.Impulse);

    }
}
