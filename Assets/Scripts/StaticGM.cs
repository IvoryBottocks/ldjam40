﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InputManager),typeof(SpawnManager))]
public class StaticGM : MonoBehaviour {

    public static InputManager input;
    public static SpawnManager spawn;
	// Use this for initialization
	void Start () {
        input = GetComponent<InputManager>();
        spawn = GetComponent<SpawnManager>();
	}
	
	
}
