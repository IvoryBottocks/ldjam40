﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public bool isOcupied;

    private void Start()
    {
        isOcupied = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        isOcupied = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isOcupied = false;
    }


}
