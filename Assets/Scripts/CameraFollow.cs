﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform objectToFollow;
    public Vector3 offsetToTarget;
    public Transform camReversePos;

	void Start () {

        offsetToTarget = objectToFollow.transform.position + new Vector3(0, 20, 1);
	}
	
	// Update is called once per frame
	void LateUpdate () {

        transform.position = objectToFollow.transform.position + offsetToTarget;
        transform.LookAt(objectToFollow);

        if (Input.GetKey(KeyCode.Q))
        {
            transform.position = camReversePos.transform.position;
            transform.LookAt(objectToFollow);
        }

	}
}
